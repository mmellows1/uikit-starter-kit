# UIKit Starter kit

This is a starter kit where you can change the style of UIKit in an easy way.

Just edit the src/scss/theme/ files.

Run the following commands

1. `npm install`
2. `gulp`

It should run on http://localhost:3000/
