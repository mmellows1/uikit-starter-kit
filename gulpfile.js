var gulp = require('gulp');
var rename = require('gulp-rename');
var sass = require('gulp-scss');
var concat = require('gulp-concat');
var browserSync = require('browser-sync').create();
var minify = require('gulp-minify');
var uglify = require('gulp-uglify');
var autoprefixer = require('gulp-autoprefixer');
var injectPartials = require('gulp-inject-partials')

var dir = {
  script: {
    src: ['src/js/vendor/*.js', 'src/js/*.js'],
    dest: 'dist/js',
    files: ['src/js/vendor/*.js', 'src/js/*.js']
  },
  style: {
    src: 'src/scss/uikit-theme.scss',
    dest: 'dist/css',
    files: ['src/scss/**/*.scss', 'src/scss/*.scss']
  },
  html: {
    src: 'tests/*.html',
    dest: 'dist/',
    files: ['tests/*.html', 'tests/**/*.html']
  }
}

gulp.task('css', function() {
  gulp.src(dir.style.src)
    .pipe(sass())
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    .pipe(minify())
    .pipe(rename('uikit.css'))
    .pipe(gulp.dest(dir.style.dest))
    .pipe(browserSync.stream());
    
  browserSync.reload()
})

gulp.task('js', function() {
  gulp.src(dir.script.src)
    .pipe(concat('app.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest(dir.script.dest));

  browserSync.reload()
})

gulp.task('html', function() {

  gulp.src(dir.html.src)
    .pipe(injectPartials())
    .pipe(gulp.dest(dir.html.dest))
})

gulp.task('default', ['css', 'js'], function() {

  browserSync.init({
      server: {
        baseDir: "./dist/"
      }
  });

  gulp.watch(dir.script.files, ['js']);
  gulp.watch(dir.style.files, ['css']);
  gulp.watch(dir.html.files, ['html'], browserSync.reload());

})
